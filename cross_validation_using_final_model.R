

## Final model with tuned hyperparameters
model_rf_final <- randomForest(data = train, 
                               critical_temp ~ .,
                               ntree = 1400,
                               mtry = 20,
                               importance = TRUE)

test$critical_temp_pred <- predict(model_rf_final, test)
sqrt(mean((test$critical_temp_pred - test$critical_temp)^2))

## Plot showing R squared
R2_test <- caret::postResample(pred = test$critical_temp_pred,
                               obs = test$critical_temp)

g <- ggplot(test, aes(critical_temp, critical_temp_pred))
g <- g + geom_point(alpha = .5)
g <- g + annotate(geom = "text", x = 1, y = 100, label = paste("R**2 = ",
                                                               round(R2_test[2], 3),
                                                               hjust = 0, vjust = 1))
g <- g + labs(x = "Actual Critical Temperature", y = "Predicted Critical Temperature", title = "Critical Temperature Correlation Plot")
g <- g + geom_smooth(se = F, method = "lm")
g

# Cross validation using the final model
model_rf_cv <- rf.crossValidation(model_rf_final, train[, 1:81], 
                                  p = 0.10,
                                  n = 10,
                                  ntree = 1400,
                                  mtry = 20) 

par(mfrow = c(2,2))
plot(model_rf_cv)  
plot(model_rf_cv, stat = "mse")
plot(model_rf_cv, stat = "var.exp")
plot(model_rf_cv, stat = "mae")
